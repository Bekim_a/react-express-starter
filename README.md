# React Express Start Pack

> Create full stack apps with React and Express. Run your client and server with a single command.

## Quick Start

```bash
# Install dependencies for Server
npm install

# Install dependencies for Client
npm run client-install

# Run the cliend & server with concurrently
npm run dev

# Run the Express Server only
npm run server

# Run the React client only
npm run client

# Server runs on http://localhost:5000 and client on http://localhost:3000
```

## App Info

### Author

Bekim Abazi

### Version

1.0.0

### License

This Project is licensed under the MIT license
