import React, { Fragment } from "react";
import logo from "./logo.svg";
import "./App.css";
import Customers from "./components/Customers";

const App = () => {
  return (
    <Fragment>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title"> React Express Starter Pack</h1>
      </header>
      <Customers />
    </Fragment>
  );
};

export default App;
